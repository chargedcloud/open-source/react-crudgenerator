<div align="center">

# @chargedcloud/react-crudgenerator

A package for you automate your CRUD operations in your React/Next.js application

[Installation](#installation) • [Pre-Configuration](#pre-configuration) • [Usage](#usage) • [Custom Styles](#custom-styles)

</div>

## Installation

You can install the package using npm:

```sh
npm install @chargedcloud/react-crudgenerator
```

## Pre-Configuration

In your backend, certain configurations are necessary for the React CRUD Generator to function optimally. Some of these configurations include:

### CRUD Generator

We use our CRUD Generator to generate queries for filtering and including data. You need to configure this package in your project:
https://www.npmjs.com/package/@chargedcloud/crudgenerator

### JSON Validator

We use JSON Schemas to generate forms and validations. Configure this package in your project:
https://www.npmjs.com/package/@igorpdasilvaa/validator

Each property must have a type and a description. The description is used to generate the input label.

Example:

```json
"properties": {
    "price": {
        "description": "Price",
        "type": "number",
        "minimum": 0,
        "errorMessage": {
            "mininum": "The price must be greater than 0"
        }
    }
}
```

## Usage

### Implementing the Read Component

In this package, we provide a DataTable component designed to display data in a table format. To use this component, import it into your file.

```jsx
// products/page.jsx

import { DataTableCRUD } from "@chargedcloud/react-crudgenerator";
```

After importing the component, you can use it in your file. There are some props that you can pass to the component:

#### options

Here you will pass some options to the DataTable. The options include:

**columns**: The columns present in your model. The array follows the format of react-data-table-component, allowing you to customize the columns as you wish.
https://react-data-table-component.netlify.app/?path=/docs/api-columns--page

Example:

```jsx
const options = {
  columns: [
    { name: "Description", column: "description" }

  // if you want pass a nested column, you can do it like this:
    {
        name: "Main Category",
        model: 'Category',
        column: "name" // column of the nested model
    }
  ],
};
```

Note: The react-crudgenerator automatically includes the nested columns in the query, so you don't need to worry about it.

**route**: The route of your model. This is used to make requests to the API.

Example:

```jsx
const options = {
    columns: [...]
    route: "products",
};
```

**filter**: The filter is passed as an object, allowing you to specify the field name you want to filter. The filter is optional.

Example:

```jsx
const options = {
    columns: [...]
    route: "products",
    filter: {
        field: 'description',
        operator: 'substring',
    }
};
```

**include**: The include is passed as an array of objects, allowing you to specify the nested model you want to include. You need to pass the route of the nested model, the columns you want to display, and the foreign key name. The include is optional.

Example:

```jsx
const options = {
    columns: [...]
    route: "manufacturers",
    include: [
        {
            route: 'products',
            foreignKey: 'ManufactoryId',
            columns: [
                { name: 'Description', columns: 'description'}
            ]
        }
    ]
};
```

Finally, pass the options to the CreateEditForm component:

```jsx
<DataTableCRUD options={options} />
```

### Implementing the Create and Update Component

In this package, we provide a Form component designed to create and update data. To use this component, import it into your file.

```jsx
// products/create/page.jsx

import { CreateEditForm } from "@chargedcloud/react-crudgenerator";
```

The form component has some props that you can pass to the component:

#### options

Here you will pass some options to the Form. The options include:

**route**: The route of your model. This is used to make requests to the API.

Example:

```jsx
const options = {
  route: "products",
};
```

**include**: The include is passed as an array of objects, allowing you to specify the nested model you want to include. You need to pass the route of the nested model, the field you want to show in the form, and the foreign key name.

Example:

```jsx
const options = {
  route: "products",
  include: [
    {
      route: "manufacturers",
      foreignKey: "ManufactoryId",
      field: "name",
    },
  ],
};
```

Finally, pass the options to the CreateEditForm component:

```jsx
<CreateEditForm options={options} />
```

Note: The CreateEditForm component automatically generates inputs and validation based on the JSON Schema of your model. Therefore, you need to configure it using the Pre-Configuration section.

## Custom Styles

You can customize the styles of the components by passing an object with the styles to the component. The pattern of the object is:

```jsx
const styles = {
  labelFilter: {
    style: {
      // CSS properties
      color: "red",
    },
    className: "form-label",
  },
};
```

### DataTableCRUD

The DataTableCRUD component has the following components that you can customize:

- **labelFilter**: The label of the filter input
- **filterInput**: The filter input
- **actionButtons**: The action buttons (create, update, and delete). You need to pass an object with the styles of each button. Example below.

```jsx
const styles = {
  actionButtons: {
    create: {
      style: {
        // CSS properties
        backgroundColor: "red",
      },
      className: "btn btn-primary",
    },
    edit: {},
    delete: {},
  },
};
```

- **datatable**: The datatable component. You need to pass an object with the patterns of the react-data-table-component. Example below.
  https://react-data-table-component.netlify.app/?path=/docs/api-custom-styles--page

```jsx
const styles = {
  datatable: {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px",
        paddingRight: "8px",
      },
    },
  },
};
```

### CreateEditForm

The CreateEditForm component has the following components that you can customize:

- **label**: The label of the input
- **submit**: The submit button

- **inputs**: The inputs of the form. You need to pass an object with the styles of each input. Example below.

```jsx
const styles = {
  inputs: {
    string: {
      style: {
        // CSS properties
        backgroundColor: "red",
      },
      className: "form-control",
    },
    boolean: {},
    number: {},
    select: {},
  },
};
```

After that, you can pass the styles to the component:

```jsx
<CreateEditForm options={options} styles={styles} />
```
