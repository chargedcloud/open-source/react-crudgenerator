import { createSelector } from '@reduxjs/toolkit';

export const contentSelector = (alias) => createSelector(
  [(state) => state.content?.data],
  (data) => data?.[alias] || { data: { rows: [], count: 0 }, params: {} },
);
