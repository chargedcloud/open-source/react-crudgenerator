import {
  DELETE_CONTENT,
  LOAD_DATA, RESET_DATA, RESET_FORM, RESET_SCHEMA,
  SET_FORM, SET_LOADING, SET_LOADING_DATA, SET_PARAMS, SET_SCHEMA,
} from '../types';

const initialState = {
  form: {},
  data: {},
  schemas: {},
  loading: false,
  loadingData: {},
};

export default function ContentReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case SET_LOADING_DATA: {
      const { alias, loading } = action.payload;

      return {
        ...state,
        loadingData: {
          ...state.loadingData,
          [alias]: loading,
        },
      };
    }
    case LOAD_DATA: {
      const { data, alias } = action.payload;

      return {
        ...state,
        data: {
          ...state.data,
          [alias]: {
            ...state.data[alias],
            data,
          },
        },
      };
    }
    case DELETE_CONTENT: {
      const { alias, id } = action.payload;

      const rows = state.data[alias].data?.rows?.filter((row) => row.id !== id);

      return {
        ...state,
        data: {
          ...state.data,
          [alias]: {
            ...state.data[alias],
            data: {
              ...state.data[alias].data,
              rows,
            },
          },
        },
      };
    }
    case SET_PARAMS: {
      const { params, alias } = action.payload;

      return {
        ...state,
        data: {
          ...state.data,
          [alias]: {
            ...state.data[alias],
            params,
          },
        },
      };
    }
    case RESET_DATA: {
      return {
        ...state,
        data: initialState.data,
      };
    }
    case SET_FORM: {
      const { alias, form } = action.payload;

      return {
        ...state,
        form: {
          ...state.form,
          [alias]: form,
        },
      };
    }
    case RESET_FORM: {
      const { alias } = action.payload;

      return {
        ...state,
        form: {
          ...state.form,
          [alias]: initialState.form[alias],
        },
      };
    }
    case SET_SCHEMA: {
      const { route, schema } = action.payload;

      return {
        ...state,
        schemas: {
          ...state.schemas,
          [route]: schema,
        },
      };
    }
    case RESET_SCHEMA: {
      const { alias } = action.payload;

      return {
        ...state,
        schemas: {
          ...state.schemas,
          [alias]: initialState.schemas[alias],
        },
      };
    }
    default:
      return state;
  }
}
