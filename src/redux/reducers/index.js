import { combineReducers } from 'redux';

const staticReducers = {};

export default function createReducer(asyncReducers) {
  return combineReducers({
    ...staticReducers,
    ...asyncReducers,
  });
}
