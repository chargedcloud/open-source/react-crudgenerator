export default function GenerateDataProvider(api) {
  return {
    getAll: async (route, params = {}, include = []) => {
      const { data } = await api.get(route, {
        params: {
          ...params,
          order: [
            { field: params.sortField, direction: params.sortDirection },
          ],
          where: JSON.stringify(params.where),
          include: JSON.stringify([
            ...include,
            ...params.include,
          ]),
        },
      });

      return data;
    },
    getById: async (route, id) => {
      const { data } = await api.get(`${route}/${id}`);

      return data;
    },
    insert: async (route, data) => {
      const { data: response } = await api.post(route, data);

      return response;
    },
    update: async (route, id, data) => {
      const { data: response } = await api.patch(`${route}/${id}`, data);

      return response;
    },
    delete: async (route, id) => {
      const { data: response } = await api.delete(`${route}/${id}`);

      return response;
    },
    getJsonSchema: async (route) => {
      const { data } = await api.post(`${route}?downloadschema=true`);

      return data;
    },
  };
}
