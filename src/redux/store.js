import { configureStore } from '@reduxjs/toolkit';
import createReducers from './reducers';

const store = configureStore({
  reducer: createReducers(),
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    thunk: true,
  }),
  devTools: true,
});

store.asyncReducers = {};

store.injectReducer = (key, reducer) => {
  store.asyncReducers[key] = reducer;
  store.replaceReducer(createReducers(store.asyncReducers));
};

export default store;
