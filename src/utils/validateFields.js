const Ajv = require('ajv');
const ajvErrors = require('ajv-errors');

const ajv = new Ajv({ allErrors: true });
ajvErrors(ajv);

function validateFields(schema, formData) {
  const validate = ajv.compile(schema);
  let errors = {};

  if (!validate(formData)) {
    validate.errors.forEach((error) => {
      let property = error.instancePath.replace('/', '');
      let { message } = error;

      if (!property || property === '') {
        property = error.params.missingProperty;
        message = 'Este campo é obrigatório';
      }

      errors = {
        ...errors,
        [property]: message,
      };
    });

    return errors;
  }

  return true;
}

module.exports = validateFields;
