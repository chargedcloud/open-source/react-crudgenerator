'use client';

import { usePathname } from 'next/navigation';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { contentSelector } from '../../redux/selectors';
import { SET_PARAMS } from '../../redux/types';

function SelectFilter({
  filter, value, handleChange, allLabel = 'Todos',
}) {
  return (
    <>
      <label 
        htmlFor={filter.field} 
        className={filter.classNameLabel ? filter.classNameLabel : `form-label `}
      >
        {filter.label}
      </label>
      <select
        className={filter.classNameInput ? filter.classNameInput : `form-select`}
        id={filter.field}
        name={filter.field}
        value={value}
        onChange={handleChange}
      >
        <option value="" selected>{allLabel}</option>
        {filter.options.map((option) => (
          <option key={option.value} value={option.value}>{option.label}</option>
        ))}
      </select>
    </>
  );
}

function InputFilter({ filter, value, handleChange }) {
  return (
    <>
      <label 
        htmlFor={filter.field} 
        className={filter.classNameLabel ? filter.classNameLabel : `form-label `}
      >{filter.label}</label>
      <input
        type={filter.type}
        className={filter.classNameInput ? filter.classNameInput : `form-control `}
        id={filter.field}
        name={filter.field}
        value={value}
        placeholder={filter.placeholder}
        onChange={handleChange}
      />
    </>
  );
}

export default function FilterForm({ filters, alias, allLabel }) {
  const searchParams = new URLSearchParams(window.location.search);
  const dispatch = useDispatch();
  const pathname = usePathname();

  const contentParams = useSelector(contentSelector(alias))?.params;

  const handleChange = (e) => {
    const { name, value } = e.target;

    searchParams.set('page', 1);

    if (value) {
      searchParams.set(name, value);
    } else {
      searchParams.delete(name);
    }

    const newUrl = `${pathname}?${searchParams.toString()}`;

    window.history.replaceState({}, '', newUrl);

    dispatch({
      type: SET_PARAMS,
      payload: {
        alias,
        params: {
          ...contentParams,
          page: 1,
          [name]: value,
        },
      },
    });
  };

  return filters.map((filter) => {
    let FilterComponent = InputFilter;
    
    if (filter.cell) {
      FilterComponent = filter.cell;
    } else if (filter.type === 'select') {
      FilterComponent = SelectFilter;
    }

    return (
      <div 
        className={filter.classNameColumn ? filter.classNameColumn  : "col-md-4 mb-3"} 
        key={filter.field}
      >
        <FilterComponent
          filter={filter}
          value={searchParams.get(filter.field) || ''}
          handleChange={handleChange}
          allLabel={allLabel}
        />
      </div>
    );
  });
}
