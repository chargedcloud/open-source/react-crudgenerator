import React from 'react';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function DeleteConfirmAlert({
  onClose,
  handleDelete,
  confirmation,
}) {
  return (
    <div
      className="custom-confirm-box p-3"
      style={{
        boxShadow: '0px 0px 10px 0px rgba(0, 0, 0, 0.2)',
      }}
    >
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">{confirmation?.title}</h5>
          <button type="button" aria-label="close" className="btn-close" onClick={onClose} />
        </div>
        <div className="modal-body">
          <p>{ confirmation?.message }</p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => {
              onClose();
              handleDelete();
            }}
          >
            {confirmation?.yes}
          </button>
          <button
            type="button"
            className="btn btn-primary ms-2"
            onClick={onClose}
          >
            {confirmation?.no}
          </button>
        </div>
      </div>
    </div>
  );
}

function DeleteButton({
  handleDelete, title = 'Excluir', style,
  confirmation,
}) {
  const handleClick = () => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <DeleteConfirmAlert
          onClose={onClose}
          handleDelete={handleDelete}
          confirmation={confirmation}
        />
      ),
    });
  };

  return (
    <button
      type="button"
      className={style?.className ?? 'btn btn-sm btn-danger'}
      onClick={handleClick}
      style={style?.style}
    >
      {title && (
        <span>{title}</span>
      )}
    </button>
  );
}

export default DeleteButton;
