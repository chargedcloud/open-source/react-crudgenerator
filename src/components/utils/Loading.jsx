'use client';

import React from 'react';
import { ClipLoader } from 'react-spinners';
import { useSelector } from 'react-redux';

export default function Loading() {
  const loading = useSelector((state) => state.content?.loading || false);

  const style = {
    position: 'fixed',
    zIndex: 9999,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    loading ? (
      <div style={style}>
        <ClipLoader size={50} color="#25D366" loading />
      </div>
    ) : null
  );
}
