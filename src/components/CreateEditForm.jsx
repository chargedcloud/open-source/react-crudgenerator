/* eslint-disable react/no-unstable-nested-components */

'use client';

import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import { useDispatch, useSelector } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import validateFields from '../utils/validateFields';
import {
  LOAD_DATA,
  RESET_DATA,
  RESET_FORM,
  RESET_SCHEMA,
  SET_FORM,
  SET_LOADING,
  SET_SCHEMA,
} from '../redux/types';
import Loading from './utils/Loading';
import ContentReducer from '../redux/reducers/ContentReducer';
import GenerateDataProvider from '../redux/providers/GenerateDataProvider';
import api from '../utils/Api';

function NumberInput({
  form, field, handleChange, styles,
}) {
  return (
    <input
      type="number"
      name={field}
      id={field}
      className={styles?.className ?? 'form-control'}
      value={form[field] || ''}
      onChange={handleChange}
      style={styles?.style}
      required
    />
  );
}

function StringInput({
  form, field, handleChange, styles,
}) {
  return (
    <input
      type="text"
      name={field}
      id={field}
      className={styles?.className || 'form-control'}
      value={form[field] || ''}
      onChange={handleChange}
      style={styles?.style}
      required
    />
  );
}

function BooleanInput({
  form, field, handleChange, styles,
}) {
  return (
    <input
      type="checkbox"
      name={field}
      id={field}
      className={styles?.className ?? 'form-check-input'}
      checked={form[field] || false}
      onChange={handleChange}
      style={styles?.style}
      required
    />
  );
}

function SelectInput({
  form, field, options, handleChange, styles,
}) {
  return (
    <select
      name={field}
      id={field}
      className={styles?.className ?? 'form-select'}
      value={form[field] || ''}
      onChange={handleChange}
      style={styles?.style}
      required
    >
      <option value="" disabled>Selecione</option>
      {options}
    </select>
  );
}

export default function CreateEditForm(userStore, axiosInstance = api) {
  return function CreateEditFormWithStore({
    options: { route, include = [], id },
    styles = {
      submit: {},
      label: {},
      inputs: {
        string: {},
        boolean: {},
        select: {},
        number: {},
      },
      errorMessage: {},
    },
  }) {
    const dispatch = useDispatch();

    const DataProvider = GenerateDataProvider(axiosInstance);

    const router = useRouter();
    const schema = useSelector((state) => state.content?.schemas[route] || null);

    useEffect(() => {
      userStore.injectReducer('content', ContentReducer);
    }, []);

    const fetchData = async () => {
      const data = await DataProvider.getJsonSchema(route);

      dispatch({
        type: SET_SCHEMA,
        payload: { route, schema: data },
      });

      if (id) {
        const response = await DataProvider.getById(route, id);

        dispatch({
          type: SET_FORM,
          payload: { alias: route, form: response },
        });
      }

      include.forEach(async (item) => {
        const response = await DataProvider.getAll(item.route);

        dispatch({
          type: LOAD_DATA,
          payload: { alias: item.route, data: response },
        });
      });
    };

    useEffect(() => {
      fetchData();

      return () => {
        dispatch({
          type: RESET_FORM,
          payload: { alias: route },
        });

        dispatch({
          type: RESET_SCHEMA,
          payload: { alias: route },
        });

        dispatch({
          type: RESET_DATA,
        });
      };
    }, []);

    function AutoForm() {
      const dataForm = useSelector((state) => state.content?.form[route] || {});
      const includeData = useSelector((state) => state.content?.data || {});

      const errors = validateFields(schema, dataForm);

      const inputs = {
        number: NumberInput,
        string: StringInput,
        boolean: BooleanInput,
        select: SelectInput,
      };

      const fields = schema.properties;

      const handleChange = (e) => {
        let { value } = e.target;
        const { type, name } = e.target;

        if (type === 'checkbox') {
          value = e.target.checked;
        }

        if (type === 'number') {
          value = Number(value);
        }

        if (type === 'select-one') {
          if (!Number.isNaN(Number(value))) {
            value = Number(value);
          }
        }

        dispatch({
          type: SET_FORM,
          payload: { alias: route, form: { ...dataForm, [name]: value } },
        });
      };

      const handleSubmit = async (e) => {
        e.preventDefault();

        if (Object.keys(errors).length > 0 || !errors) {
          return;
        }

        dispatch({
          type: SET_LOADING,
          payload: true,
        });

        if (id) {
          await DataProvider.update(route, id, dataForm);
        } else {
          await DataProvider.insert(route, dataForm);
        }

        dispatch({
          type: SET_LOADING,
          payload: false,
        });

        router.back();
      };

      const form = Object.keys(fields).map((key) => {
        const field = fields[key];

        const includedField = include.find((item) => item.foreignKey === key);

        if (includedField) {
          const { route: itemRoute, field: itemField } = includedField;

          const { data } = includeData[itemRoute] || {};

          const options = data?.rows && data.rows.map((row) => (
            <option key={row.id} value={row.id}>{row[itemField]}</option>
          ));

          return (
            <div className="mb-3">
              <label
                htmlFor={key}
                className={styles?.label?.className ?? 'form-label'}
                style={styles?.label?.style}
              >
                {field?.description ?? itemRoute}
              </label>
              {React.createElement(
                inputs.select,
                {
                  form: dataForm,
                  field: key,
                  handleChange,
                  options,
                  styles: styles?.inputs?.select,
                },
              )}
              {errors[key] && (
              <div
                className={styles?.errorMessage?.className ?? 'invalid-feedback d-block'}
                style={styles?.errorMessage?.style}
              >
                {errors[key]}
              </div>
              )}
            </div>
          );
        }

        return (
          <div className="mb-3">
            <label
              htmlFor={key}
              className={styles?.label?.className ?? 'form-label'}
              style={styles?.label?.style}
            >
              {field?.description ?? key}

            </label>
            {React.createElement(
              inputs[field.type],
              {
                form: dataForm,
                field: key,
                handleChange,
                styles: styles?.inputs[field.type],
              },
            )}
            {errors[key] && (
            <div
              className={styles?.errorMessage?.className ?? 'invalid-feedback d-block'}
              style={styles?.errorMessage.style}
            >
              {errors[key]}
            </div>
            )}
          </div>
        );
      });

      return (
        <>
          <Loading />
          <form onSubmit={handleSubmit}>
            {form}
            <button
              type="submit"
              className={styles?.submit?.className ?? 'btn btn-success'}
              style={styles?.submit?.style}
            >
              Enviar
            </button>
          </form>
          <ToastContainer
            position="bottom-right"
            autoClose={2500}
            theme="colored"
          />
        </>
      );
    }

    return schema && <AutoForm schema={schema} route={route} />;
  };
}
