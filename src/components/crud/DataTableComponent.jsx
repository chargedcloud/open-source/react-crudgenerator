/* eslint-disable no-continue */
/* eslint-disable no-restricted-syntax */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable jsx-a11y/label-has-associated-control */

'use client';

import React, { useEffect } from 'react';
import DataTable from 'react-data-table-component';
import { debounce } from 'lodash';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { usePathname } from 'next/navigation';
import { ClipLoader } from 'react-spinners';
import GenerateDataProvider from '../../redux/providers/GenerateDataProvider';
import {
  DELETE_CONTENT,
  LOAD_DATA, RESET_DATA, SET_LOADING, SET_LOADING_DATA, SET_PARAMS,
} from '../../redux/types';
import DeleteButton from '../utils/DeleteButton';
import ContentReducer from '../../redux/reducers/ContentReducer';
import { contentSelector } from '../../redux/selectors';
import FilterForm from '../utils/FilterForm';

function DataTableComponent(store, axiosInstance) {
  return function DataTableComponentWithStore({
    styles = {
      labelFilter: {},
      filterInput: {},
      datatable: {},
      actionButtons: {},
    },
    options: {
      route,
      columns,
      filters = [], actionButtons, showRegisterButton = true,
      sortField: defaultSortField, sortDirection: defaultSortDirection,
    },
    labels = {
      rowsPerPageText: 'Registros por página:',
      rangeSeparatorText: 'de',
      noDataComponent: 'Nenhum registro encontrado',
      editButtonLabel: 'Editar',
      deleteButtonLabel: 'Excluir',
      createButtonLabel: 'Cadastrar',
      confirmation: {
        title: 'Confirmação',
        message: 'Você tem certeza que gostaria de excluir este item?',
        yes: 'Sim',
        no: 'Não',
      },
      allLabel: 'Todos',
    },
    alias,
    defaultParams = {},
    where,
    expandableRows,
    expandableRowsComponent,
    fixedHeader = false,
    fixedHeaderScrollHeight,
  }) {
    const searchParams = new URLSearchParams(window.location.search);

    const pathname = usePathname();
    const dispatch = useDispatch();

    const aliasRoute = alias || route;

    const DataProvider = GenerateDataProvider(axiosInstance);

    const contentData = useSelector(contentSelector(aliasRoute))?.data;
    const contentParams = useSelector(contentSelector(aliasRoute))?.params;
    const loading = useSelector((state) => state.content?.loadingData?.[aliasRoute] || false);

    const queryParams = {
      page: Number(searchParams.get('page')) || 1,
      pageLimit: Number(searchParams.get('pageLimit')) || 20,
      sortField: searchParams.get('sortField') || defaultSortField,
      sortDirection: searchParams.get('sortDirection') || defaultSortDirection,
      ...contentParams,
    };

    const setLoading = (isLoading) => dispatch({
      type: SET_LOADING_DATA,
      payload: {
        loading: isLoading,
        alias: aliasRoute,
      },
    });

    const setParamsState = (name, value) => dispatch({
      type: SET_PARAMS,
      payload: {
        alias: aliasRoute,
        params: {
          ...contentParams,
          [name]: value,
        },
      },
    });

    useEffect(() => {
      if (store) {
        store.injectReducer('content', ContentReducer);
      }

      return () => {
        dispatch({
          type: RESET_DATA,
        });
      };
    }, []);

    const fetchData = async (params) => {
      const nestedColumn = columns.filter((column) => column.model);

      const include = nestedColumn.map((column) => ({
        model: column.model,
      }));

      const response = await DataProvider.getAll(
        route,
        {
          ...params,
          ...defaultParams,
        },
        include,
      );

      dispatch({
        type: LOAD_DATA,
        payload: {
          data: response,
          alias: aliasRoute,
        },
      });
    };

    const filtersFields = filters.map((filter) => filter.field);

    useEffect(() => {
      setLoading(true);

      const filtersQuery = {
        where: {
          conditions: [],
        },
        include: [],
      };

      if (where) {
        filtersQuery.where.conditions.push(where);
      }

      for (const [key, value] of searchParams.entries()) {
        if (filtersFields.includes(key)) {
          const filter = filters.find((f) => f.field === key);

          if (filter.model) {
            filtersQuery.include.push({
              model: filter.model,
              where: {
                conditions: [{
                  field: filter.field,
                  operator: filter.operator,
                  value,
                }],
              },
            });

            continue;
          }

          filtersQuery.where.conditions.push({
            field: filter.field,
            operator: filter.operator,
            value,
          });

          continue;
        }

        filtersQuery[key] = value;
      }

      const debounceLoadData = debounce(() => {
        fetchData({ ...filtersQuery }).then(() => {
          setLoading(false);
        });
      }, 600);

      if (route) {
        debounceLoadData();
      }

      return () => {
        setLoading(false);
        debounceLoadData.cancel();
      };
    }, [
      queryParams.page,
      queryParams.pageLimit,
      queryParams.sortField,
      queryParams.sortDirection,
      ...filtersFields.map((field) => queryParams[field]),
    ]);

    const setFilters = (key, value) => {
      if (value) {
        searchParams.set(key, value);
      } else {
        searchParams.delete(key);
      }

      const newUrl = `${pathname}?${searchParams.toString()}`;

      window.history.replaceState({}, '', newUrl);

      setParamsState(key, value);
    };

    const handleSort = (column, direction) => {
      if (!column.sortField) return;

      if (column.sortField === queryParams.sortField
        && direction === queryParams.sortDirection) return;

      searchParams.set('sortField', column.sortField);
      searchParams.set('sortDirection', direction);

      const newUrl = `${pathname}?${searchParams.toString()}`;

      window.history.replaceState({}, '', newUrl);

      dispatch({
        type: SET_PARAMS,
        payload: {
          alias: aliasRoute,
          params: {
            ...contentParams,
            sortField: column.sortField,
            sortDirection: direction,
          },
        },
      });
    };

    const handlePageChange = (newPage) => {
      if (newPage === queryParams.page) return;

      setFilters('page', newPage);
    };

    const handlePerRowsChange = (newPerPage) => {
      if (newPerPage === queryParams.pageLimit) return;

      searchParams.set('page', 1);
      searchParams.set('pageLimit', newPerPage);

      const newUrl = `${pathname}?${searchParams.toString()}`;
      window.history.replaceState({}, '', newUrl);

      dispatch({
        type: SET_PARAMS,
        payload: {
          alias: aliasRoute,
          params: {
            ...contentParams,
            page: 1,
            pageLimit: newPerPage,
          },
        },
      });
    };

    const handleDelete = async (id) => {
      dispatch({
        type: SET_LOADING,
        payload: true,
      });

      await DataProvider.delete(route, id);

      dispatch({
        type: SET_LOADING,
        payload: false,
      });

      dispatch({
        type: DELETE_CONTENT,
        payload: {
          alias: aliasRoute,
          id,
        },
      });
    };

    const datatableColumns = columns.map((column) => {
      const dateField = ['createdAt', 'updatedAt', 'deletedAt'];

      const fieldColumn = {
        selector: (row) => {
          if (dateField.includes(column.column)) {
            return new Date(row[column.column]).toLocaleString();
          }

          if (column.model) {
            return row[column.model][column.column];
          }

          return row[column.column];
        },
        ...column,
      };

      if (column.model) {
        return fieldColumn;
      }

      const columnObject = {
        ...fieldColumn,
      };

      if (column.column) {
        columnObject.sortable = true;
        columnObject.sortFunction = () => {};
        columnObject.sortField = column.column;
      }
      if(column.sortable === false) {
        columnObject.sortable = false;
        delete columnObject.sortFunction;
        delete columnObject.sortField;
      }

      return columnObject;
    });

    const { create, edit, delete: deleteButton } = styles.actionButtons || {};

    const actionsColumn = actionButtons ?? {
      name: 'Ações',
      selector: (row) => (
        <div className="d-flex gap-2">
          <Link href={`${route}/edit/${row.id}`}>
            <button
              type="button"
              style={edit?.style}
              className={edit?.className ?? 'btn btn-sm btn-warning'}
            >
              {labels.editButtonLabel}
            </button>
          </Link>
          <DeleteButton
            handleDelete={() => handleDelete(row.id)}
            style={deleteButton}
            title={labels.deleteButtonLabel}
            confirmation={labels.confirmation}
          />
        </div>
      ),
    };

    const columnFindIndex = (column) => (columns.findIndex((c) => c.sortField === column)) + 1;

    function ShowDataTable() {
      if (loading) {
        return (
          <div className="p-5 d-flex justify-content-center">
            <ClipLoader color="#36d7b7" />
          </div>
        );
      }

      return (
        <DataTable
          columns={[...datatableColumns, actionsColumn]}
          data={contentData.rows}
          pagination
          paginationServer
          sortServer
          paginationTotalRows={contentData.count}
          paginationDefaultPage={queryParams.page}
          paginationPerPage={queryParams.pageLimit}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handlePerRowsChange}
          onSort={handleSort}
          expandableRows={expandableRows}
          expandableRowsComponent={expandableRowsComponent}
          customStyles={styles?.datatable}
          noDataComponent={<div>{labels.noDataComponent}</div>}
          paginationRowsPerPageOptions={[20, 30, 50, 100, 200]}
          defaultSortFieldId={columnFindIndex(queryParams.sortField)}
          defaultSortAsc={queryParams.sortDirection === 'asc'}
          paginationComponentOptions={{
            rowsPerPageText: labels.rowsPerPageText,
            rangeSeparatorText: labels.rangeSeparatorText,
            noRowsPerPage: false,
            selectAllRowsItem: false,
          }}
          fixedHeader={fixedHeader}
          fixedHeaderScrollHeight={fixedHeaderScrollHeight}
        />
      );
    }

    return (
      <div>
        { showRegisterButton && (
        <Link href={`${route}/create`}>
          <button
            type="button"
            className={create?.className ?? 'btn btn-success float-end'}
            style={create?.style}
          >
            {labels.createButtonLabel}
          </button>
        </Link>
        )}
        <div className="row mb-3 w-100">
          <FilterForm
            filters={filters}
            alias={aliasRoute}
            allLabel={labels.allLabel}
          />
        </div>
        <ShowDataTable />
      </div>
    );
  };
}

export default DataTableComponent;
