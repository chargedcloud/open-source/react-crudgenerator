'use client';

import React from 'react';
import { ToastContainer } from 'react-toastify';
import DataTableComponent from './crud/DataTableComponent';
import Loading from './utils/Loading';
import api from '../utils/Api';

function DataTableCRUD(userStore, axiosInstance = api) {
  const DataTableComponentWithStore = DataTableComponent(userStore, axiosInstance);

  return function DataTableWithStore({
    options,
    alias,
    styles,
    where,
    defaultParams,
    labels,
    fixedHeader,
    fixedHeaderScrollHeight,
  }) {
    const renderExpandableRows = (rowData, include) => include && include.map((option) => (
      <div className="py-4">
        <DataTableComponentWithStore
          alias={`${option?.route}/${rowData.id}`}
          styles={styles}
          options={option}
          where={{
            field: option.foreignKey,
            value: rowData.id,
            operator: 'eq',
          }}
          expandableRows={option.include}
          expandableRowsComponent={({ data }) => renderExpandableRows(data, option.include)}
          labels={labels}
          fixedHeader={fixedHeader}
          fixedHeaderScrollHeight={fixedHeaderScrollHeight}
        />
      </div>
    ));

    return (
      <div>
        <Loading />
        <DataTableComponentWithStore
          alias={alias}
          defaultParams={defaultParams}
          styles={styles}
          options={options}
          expandableRows={options?.include}
          expandableRowsComponent={
            ({ data: rowData }) => renderExpandableRows(rowData, options?.include)
          }
          where={where}
          labels={labels}
          fixedHeader={fixedHeader}
          fixedHeaderScrollHeight={fixedHeaderScrollHeight}
        />
        <ToastContainer
          position="bottom-right"
          autoClose={2500}
          theme="colored"
        />
      </div>
    );
  };
}

export default DataTableCRUD;
